#!/bin/bash

# Be sure to change /path/to/rsync_ignore and /path/to /external/drive/destination/ to the appropriate directories.

rsync -aAvxXPn --exclude-from=/path/to/rsync.system.exclude.list --delete / /external/drive/destination/
#
# -a, --archive (includes --links)
# 		-l, --links		copy symlinks as symlinks
#
# -A --acls        preserve ACLs (implies --perms)
#                       Linux Access Control Lists (ACLs) allow the application of a more specific set of permissions
#                       to a file or directory without (necessarily) changing the base ownership and permissions. 
#
# -v --verbose 	This  option increases the amount of information you 
# 				        are given during the transfer.
#
# -x, --one-file-system       don't cross file system boundaries
#
# -P    The  -P  option  is  equivalent to --partial --progress.  
# 		--partial          Keeps the partial file
# 		--progress      Shows the progress of the transfer.
#
# -n, 	--dry-run Perform a trial run with no changes made
#
# --delete                delete extraneous files from destination directories
#
# --exclude=PATTERN       exclude files matching PATTERN
#	rsync -av --exclude={'*.txt','dir3','dir4'} sourcedir/ destinationdir/
# 
# --exclude-from=FILE     read exclude patterns from FILE
