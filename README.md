# rsync.system

Use rsync to back up your system. Rsync, or Remote Sync, is a command-line tool that transfers files and directories to local and remote destinations. 

These scripts are provided in the hope that they will be useful by Tom Furnari (www.linuxexperience.xyz)